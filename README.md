# Snakes And Ladders - Unity Implementation #

This is an example learning project that implements the board game Snakes and Ladders using Unity 5.

### What is this repository for? ###

* Snakes and Ladders implemented with Unity 5
* Version 0.1

### How do I get set up? ###

* Download repository
* Open project using Unity 5.4 or higher.

### Who do I talk to? ###

* kumilanka@gmail.com