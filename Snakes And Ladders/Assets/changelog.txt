v 0.1 (17.10.2016)
------------------

Features
* Crude board representation using Unity UI Canvas
* Board size can be set between 8x8 and 12x12
* Instantaited grid squares that form a continuous path on the game board
* Player piece that moves on a timer from the start to the goal position (no interaction yet)
