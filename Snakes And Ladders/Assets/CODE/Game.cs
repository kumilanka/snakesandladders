﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Game : MonoBehaviour
{
    [Range(8, 12)] // for the inspector, limit board sizes to 8x8 -> 12x12
    public int boardSize = 8;

    public GameObject boardSquarePrefab;
    public Transform boardGridParent;
    public Transform backgroundTransform;
    public GridLayoutGroup boardLayoutGrid;
    public Text dieRollResultLabel;
    public Button dieRollButton;

    public Text playerAmountLabel;

    public GameObject startWindow;

    public int EndPosition
    {
        get
        {
            return boardSize * boardSize;
        }
    }

    public List<KeyValuePair<int, GameObject>> playerPath = new List<KeyValuePair<int, GameObject>>(); // store the grid squares in order in this list, from start to finish

    public GameObject playerPrefab;
    public GameObject snakePrefab;
    public GameObject ladderPrefab;

    public List<SnakeLadder> snakesLadders = new List<SnakeLadder>(); // list for the snakes and the ladders

    private int numberOfPlayers = 1;
    public List<Player> players = new List<Player>(); // store a list of players here
    public int currentPlayerIndex;

    public bool gameRunning;

    public static Game instance;
    public void Awake()
    {
        instance = this; // this is a pseudo-singleton, which is helpful when calling the game script's functions from other scripts.
    }

	// Use this for initialization
	void Start ()
    {
        startWindow.SetActive(true); // activate start window if it's not active already
    }

    private IEnumerator InitializeAfterTime()
    {
        yield return null;

        // calculate desired size for a single square
        float boardSquareSize = boardGridParent.GetComponent<RectTransform>().rect.height; 
        boardSquareSize /= boardSize;

        // for the grid layout of the board, set the layout grid's size to fit all the squares
        boardLayoutGrid.cellSize = new Vector2(boardSquareSize, boardSquareSize); // assume width and height are the same for the board

        //InitializeBoard();
        yield return null;

        // for every position on the board
        for (int i = 0; i < boardSize * boardSize; i++)
        {
            // instantiate a grid square. The squares are automatically resized by the layout grid element of the parent object
            GameObject go = Instantiate(boardSquarePrefab, boardGridParent) as GameObject;
            //go.transform.localScale = Vector2.one;
        }
    }

    /// <summary>
    /// Called when the game begins, depending on what the board size is, instantiate a bunch of grid squares to act as the track the players traverse
    /// </summary>
    private void InitializeBoard()
    {
        // calculate desired size for a single square, so they will all fit neatly and take all the board space
        float boardSquareSize = boardGridParent.GetComponent<RectTransform>().rect.height;
        boardSquareSize /= boardSize;

        // for the grid layout of the board, set the layout grid's size to fit all the squares
        boardLayoutGrid.cellSize = new Vector2(boardSquareSize, boardSquareSize); // assume width and height are the same for the board

        int gridIndex = 1; // start counting the board positions from 1
        bool reverseRow = false; // use a boolean to flip every other row
        
        // for every position on the board
        for (int width = 0; width < boardSize; width++)
        {
            for (int height = 0; height < boardSize; height++)
            {
                // instantiate a grid square
                GameObject go = Instantiate(boardSquarePrefab, boardGridParent) as GameObject;

                // set an index number for the board pieces to determine the order of them
                int index = gridIndex;

                if (reverseRow) // in case of a reverse row, since the grid layout places squares in rows from left to right, and bottom to top
                {
                    // we must reverse the index numbers for this row, so that they go from right to left (and thus create a continuous path on the board)
                    int reverseIndex = boardSize - height;
                    index = gridIndex + (reverseIndex - height) - 1;
                }
                go.GetComponentInChildren<Text>().text = index.ToString(); // set the label for the square to say its index number so we can see where the player's path goes on the board

                playerPath.Add(new KeyValuePair<int, GameObject>(index, go)); // add the square to the path list (we will sort the list later to get the correct order)


                gridIndex++;// grow the index by one for every square placed
            }

            reverseRow = !reverseRow; // every other row is flipped
        }

        playerPath.Sort((x, y) => x.Key.CompareTo(y.Key)); // sort the player path so that the steps are in order

        // todo: create a number of snakes and ladders
        snakesLadders.Add(InstantiateSnakeLadder(snakePrefab, 63, 46));
        snakesLadders.Add(InstantiateSnakeLadder(snakePrefab, 37, 22));
        snakesLadders.Add(InstantiateSnakeLadder(snakePrefab, 31, 16));
        snakesLadders.Add(InstantiateSnakeLadder(snakePrefab, 55, 39));
        snakesLadders.Add(InstantiateSnakeLadder(snakePrefab, 12, 2));

        snakesLadders.Add(InstantiateSnakeLadder(ladderPrefab, 5, 30));
        snakesLadders.Add(InstantiateSnakeLadder(ladderPrefab, 21, 28));
        snakesLadders.Add(InstantiateSnakeLadder(ladderPrefab, 34, 46));
        snakesLadders.Add(InstantiateSnakeLadder(ladderPrefab, 38, 60));
        snakesLadders.Add(InstantiateSnakeLadder(ladderPrefab, 23, 40));

    }

    private SnakeLadder InstantiateSnakeLadder(GameObject prefab, int startPos, int endPos)
    {
        GameObject go = Instantiate(prefab, backgroundTransform) as GameObject;
        SnakeLadder snakeLadder = go.GetComponent<SnakeLadder>();
        snakeLadder.startPosition = startPos;
        snakeLadder.endPosition = endPos;

        return snakeLadder;
    }

    private void InitializePlayers(int numberOfPlayers)
    {
        for (int i = 0; i < numberOfPlayers; i++)
        {
            // instantiate the player's piece
            GameObject go = Instantiate(playerPrefab, backgroundTransform) as GameObject;

            // get the player script and initialize the values
            Player p = go.GetComponent<Player>();
            p.Initialize(i + 1, GetPlayerColor(i + 1));
            players.Add(p); // add to the list of players
        }
    }

    private Color GetPlayerColor(int playerIndex)
    {
        switch (playerIndex)
        {
            case 1:
                return Color.red;
            case 2:
                return Color.blue;
            case 3:
                return Color.green;
            case 4:
                return Color.yellow;
            default:
                return new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)); // if there are more players, return a random color for them
        }

    }

    public GameObject GetBoardSquare(int index)
    {
        return playerPath[index - 1].Value;
    }


    public void OnPlayerLanded(Player player, int boardPosition)
    {
        // if player landed at the bottom of a ladder, or the head of a snake, transport them to the other end of the snake or ladder
        foreach (SnakeLadder snl in snakesLadders)
        {
            if (boardPosition == snl.startPosition)
                player.Transport(snl.endPosition);
        }

        // change the current player in sequence
        if (currentPlayerIndex < players.Count - 1)
            currentPlayerIndex++;
        else
            currentPlayerIndex = 0;

    }

    public void OnPlayerReachedGoal(Player player)
    {
        Debug.Log(string.Format("Yay! Player {0} has reached the goal and won!", player.playerIndex));
        UnityEngine.SceneManagement.SceneManager.LoadScene("Game"); // reload the scene to play a new game

        // todo: display end window to celebrate the winner
    }

	// Update is called once per frame
	void Update ()
    {
        if (gameRunning)
        {
            // set active and inactive the die roll button and the label displaying results depending on if the current player is already moving (dice was thrown)
            if (!players[currentPlayerIndex].IsMoving)
            {
                //dieRollResultLabel.text = ""; // don't show the die roll result if the current player is moving
                dieRollButton.gameObject.SetActive(true);
                dieRollButton.GetComponentInChildren<Text>().text = string.Format("Player {0}, Roll Dice!", players[currentPlayerIndex].playerIndex);
            }
            else
            {
                dieRollButton.gameObject.SetActive(false);
            }
        }
	}

    /// <summary>
    /// Called by whatever it is that initiates the die rolling procedure, currently the roll dice button.
    /// </summary>
    public void OnDiceRoll()
    {
        int result = Random.Range(1, 7); // roll 6-sided die

        // cheats for testing
        if (Input.GetKey(KeyCode.LeftShift))
            result = 1;

        if (Input.GetKey(KeyCode.LeftControl))
            result = 6;

        players[currentPlayerIndex].Move(result);

        dieRollResultLabel.text = result.ToString();
    }

    public void OnPlayerAmountSliderValueChanged(Slider slider)
    {
        playerAmountLabel.text = slider.value.ToString();
        numberOfPlayers = (int)slider.value;
    }

    public void OnNewGameButtonPressed()
    {
        // when the main menu start new game button is pressed, hide the start window
        startWindow.SetActive(false);

        // and start a new game
        StartNewGame();
    }

    public void StartNewGame()
    {
        // when game starts, initialize the board
        InitializeBoard();

        //StartCoroutine(InitializeAfterTime());
        InitializePlayers(numberOfPlayers);

        gameRunning = true;
    }
}
