﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public int playerIndex; // index of the player (1, 2, 3, 4)
    public Color playerColor; // color of the player piece
    public int currentPosition; // the current position on the board where the player is
    public int targetPosition; // the target position on the board where the player is to move (if they are moving that is)
    public Image playerGraphic; // the graphic representing the player piece on the board
    public Text playerIndexLabel; // the text label displaying the player index

    public bool IsMoving
    {
        get
        {
            return currentPosition != targetPosition;
        }
    }

    public void Initialize(int playerIndex, Color playerColor)
    {
        currentPosition = 1; // start in the first position
        targetPosition = 1;
        this.playerIndex = playerIndex;
        this.playerColor = playerColor;

        // set the player piece's color and text to display the index
        playerGraphic.color = playerColor;
        playerIndexLabel.text = playerIndex.ToString();
    }

    /// <summary>
    /// Move forward on the game board.
    /// </summary>
    /// <param name="numberOfSteps">Number of board positions to advance</param>
    public void Move(int numberOfSteps)
    {
        targetPosition = currentPosition + numberOfSteps;
        moveTimer = Time.time + 0.5f;
    }

    /// <summary>
    /// Instantly move piece to a board position
    /// </summary>
    /// <param name="destinationPosition"></param>
    public void Transport(int destinationPosition)
    {
        // set new current position
        this.currentPosition = destinationPosition;
        this.targetPosition = destinationPosition;

        // move the piece object on the board on top of the correct square
        transform.position = Game.instance.GetBoardSquare(currentPosition).transform.position;
    }

	// Use this for initialization
	void Start ()
    {

    }

    private float moveTimer;
	// Update is called once per frame
	void Update ()
    {
        if (currentPosition < targetPosition)
        {
            if (moveTimer < Time.time)
            {
                moveTimer = Time.time + 0.5f;

                if (currentPosition < Game.instance.boardSize * Game.instance.boardSize)
                    currentPosition++;

                transform.position = Game.instance.GetBoardSquare(currentPosition).transform.position;

                if (currentPosition == targetPosition)
                    Game.instance.OnPlayerLanded(this, currentPosition);

                // for now, player doesn't have to reach the end point on an even die roll, any roll going past the end will do
                if (currentPosition == Game.instance.EndPosition)
                {
                    Game.instance.OnPlayerReachedGoal(this);
                    targetPosition = currentPosition;
                }
            }
        }
        else
            Transport(currentPosition);
    }
}
