﻿using UnityEngine;
using System.Collections;

public class SnakeLadder : MonoBehaviour
{
    public enum Modes
    {
        Snake,
        Ladder
    }

    public Modes mode;

    public int startPosition;
    public int endPosition;

	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        // calculate the position, rotation and dimensions of the snake or ladder graphic as follows:

        // Get world position of start and end point
        Vector3 startPos = Game.instance.GetBoardSquare(startPosition).transform.position;
        Vector3 endPos = Game.instance.GetBoardSquare(endPosition).transform.position;

        // Get direction vector from start toward end
        Vector3 dir = endPos - startPos;
        dir /= 2; // calculate half-distance

        // set graphic's position to be in the midpoint between the two
        transform.position = startPos + dir;

        // rotate the graphic so it is angled between the start and end points
        transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + 90);

        // set the dimensions of the graphic's bounding box so the image "reaches" between the two board positions
        GetComponent<RectTransform>().sizeDelta = new Vector2(dir.magnitude, dir.magnitude * 2);

    }
}
